//
//  Programador.swift
//  Classes
//
//  Created by Vitor Lentos on 08/06/21.
//

import Foundation

class Programador: Funcionario {
    
    var plataforma:String;
    
    init(nome:String, salario:Double, cpf:String, plataforma:String) {
        self.plataforma = plataforma;
        super.init(nome: nome, salario: salario, cpf: cpf);
    }
    
    override func calcularBonus() -> Double {
        let bonus:Double;
        bonus = salario*0.2;
        return bonus
    }
    
}

