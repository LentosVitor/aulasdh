//
//  Animal.swift
//  Classes
//
//  Created by Vitor Lentos on 08/06/21.
//

import Foundation

class Animal {
    
    var nome:String;
    var cor: String;
    
    init(nome:String, cor:String) {
        self.nome = nome;
        self.cor = cor;
    }
    
    func emitirSom() -> String {
        return "O animal está emitindo som";
    }
    
    func comer(){
        print("O animal se alimentou");
    }
    
}

