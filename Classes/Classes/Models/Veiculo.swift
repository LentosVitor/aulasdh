//
//  Veiculo.swift
//  Classes
//
//  Created by Vitor Lentos on 01/06/21.
//

import Foundation

class Veiculo {
    
    var qtdRodas: Int?
    var cor: String?
    var marca: String?
    var motor: Float?
    var combustivel: [String]?
    var ano: Int?
    var modelo: String?
    
    init(qtdRodas: Int?, cor: String?, marca: String?, motor: Float?, combustivel: [String]?, ano: Int?, modelo: String?) {
        self.qtdRodas = qtdRodas;
        self.cor = cor;
        self.marca = marca;
        self.motor = motor;
        self.combustivel = combustivel;
        self.ano = ano;
        self.modelo = modelo;
    }
    
    init(){
        
    }
    
    func acelerar() {
        print("Acelerar....");
    }
    
    func freiar() {
        print("Freiar....");
    }
    
    func estacionar() {
        print("Estacionar....");
    }
    
}


