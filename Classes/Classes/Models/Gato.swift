//
//  Gato.swift
//  Classes
//
//  Created by Vitor Lentos on 08/06/21.
//

import Foundation

class Gato: Animal {
    
    
    override init(nome:String, cor:String) {
        super.init(nome: nome, cor: cor);
    }
    
    override func emitirSom() -> String {
        return "Miaaaaar"
    }
    
    override func comer() {
        print("Comeu peixe");
    }
    
    
    
}
