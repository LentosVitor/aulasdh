//
//  Funcionario.swift
//  Classes
//
//  Created by Vitor Lentos on 08/06/21.
//

import Foundation

class Funcionario {
    
    var nome:String;
    var salario:Double;
    var cpf:String;
    
    init(nome:String, salario:Double, cpf:String) {
        self.nome = nome;
        self.salario = salario;
        self.cpf = cpf;
    }
    
    func calcularBonus() -> Double {
        return self.salario;
    }
    
}

