//
//  Carro.swift
//  Classes
//
//  Created by Vitor Lentos on 25/05/21.
//

import Foundation

class Carro:Veiculo {
    
    var qtdPortas: Int?
    var cambioAutomatico: Bool?
    var cambioManual: Bool?
    
    init(qtdRodas: Int?, cor: String?, qtdPortas: Int?, marca: String?, motor: Float?, combustivel: [String]?, ano: Int?, modelo: String?, cambioAutomatico: Bool?, cambioManual: Bool?) {
        
        super.init();
        
        self.qtdRodas = qtdRodas;
        self.cor = cor;
        self.qtdPortas = qtdPortas;
        self.marca = marca;
        self.motor = motor;
        self.combustivel = combustivel;
        self.ano = ano;
        self.modelo = modelo;
        self.cambioAutomatico = cambioAutomatico;
        self.cambioManual = cambioManual;
        
        
    }
    
    func imprimir() {
        print("Imprimindo:  \(self.modelo) - \(self.motor) - \(self.cor)");
        print("---------------");
    }

}

