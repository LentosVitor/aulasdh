//
//  Designer.swift
//  Classes
//
//  Created by Vitor Lentos on 08/06/21.
//

import Foundation

class Designer: Funcionario {
    
    var ferramentaPreferida: String;
    
    init(nome:String, salario:Double, cpf:String, ferramentaPreferida:String) {
        self.ferramentaPreferida = ferramentaPreferida;
        super.init(nome: nome, salario: salario, cpf: cpf);
    }
    
    override func calcularBonus() -> Double {
        let bonus:Double;
        bonus = salario*0.15;
        return bonus
    }
    
}
