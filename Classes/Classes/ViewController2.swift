//
//  ViewController2.swift
//  Classes
//
//  Created by Vitor Lentos on 08/06/21.
//

import UIKit

class ViewController2: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        var func1: Programador = Programador(nome: "Vitor", salario: 1000, cpf: "373.373.373-04", plataforma: "Swift");
        var func2:Designer = Designer(nome: "Julia", salario: 1000, cpf: "372.372.372-04", ferramentaPreferida: "Maya");
        
        print(func1.calcularBonus());
        print(func2.calcularBonus());
        
        
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
