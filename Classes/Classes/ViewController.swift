//
//  ViewController.swift
//  Classes
//
//  Created by Vitor Lentos on 25/05/21.
//

import UIKit

class ViewController: UIViewController {
    
    //MARK: Outlets
    @IBOutlet weak var modeloLabel: UILabel!
    @IBOutlet weak var motorLabel: UILabel!
    @IBOutlet weak var corLabel: UILabel!
    @IBOutlet weak var qtdPortasLabel: UILabel!
    @IBOutlet weak var qtdRodasLabel: UILabel!
    @IBOutlet weak var combustivelLabel: UILabel!
    @IBOutlet weak var anoLabel: UILabel!
    @IBOutlet weak var cambioLabel: UILabel!
    @IBOutlet weak var marcaLabel: UILabel!
    
    @IBOutlet weak var btn01: UIButton!
    @IBOutlet weak var btn02: UIButton!
    @IBOutlet weak var btn03: UIButton!
    @IBOutlet weak var btn04: UIButton!
    
    //MARK: Vars
    var carroEscolhido: Int = 0;
    var carros : [Carro] = [];
    var motos : [Moto] = [];
    

    //MARK: ViewController
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.modeloLabel.backgroundColor = UIColor.red;
        
        var carro1: Carro = Carro(qtdRodas: 1, cor: "Azul", qtdPortas: 4, marca: "GM", motor: 1.5, combustivel: ["Gasolina"], ano: 1998, modelo: "Monza tubarão", cambioAutomatico: false, cambioManual: true);
        var carro2: Carro = Carro(qtdRodas: 2, cor: "Preto", qtdPortas: 4, marca: "GM", motor: 1.5, combustivel: ["Gasolina"], ano: 1998, modelo: "Monza tubarão", cambioAutomatico: false, cambioManual: true);
        var carro3: Carro = Carro(qtdRodas: 3, cor: "Vermelho", qtdPortas: 4, marca: "GM", motor: 1.5, combustivel: ["Gasolina"], ano: 1998, modelo: "Monza tubarão", cambioAutomatico: false, cambioManual: true);
        var carro4: Carro = Carro(qtdRodas: 4, cor: "Rosa", qtdPortas: 4, marca: "GM", motor: 1.5, combustivel: ["Gasolina"], ano: 1998, modelo: "Monza tubarão", cambioAutomatico: false, cambioManual: true);
        var carro5: Carro = Carro(qtdRodas: 5, cor: "Branco", qtdPortas: 4, marca: "GM", motor: 1.5, combustivel: ["Gasolina"], ano: 1998, modelo: "Monza tubarão", cambioAutomatico: false, cambioManual: true);
        
        self.carros.append(carro1);
        self.carros.append(carro2);
        self.carros.append(carro3);
        self.carros.append(carro4);
        self.carros.append(carro5);
        
        let moto1: Moto = Moto(qtdRodas: 1, cor: "Preta", marca: "Honda", motor: 1.5, combustivel: ["Gasolina"], ano: 2020, modelo: "Sei lá", cilindradas: 150, tipoFreio: "Normal", guidao: "Normal", esportiva: false, pedal: "Normal");
        let moto2: Moto = Moto(qtdRodas: 2, cor: "Preta", marca: "Honda", motor: 1.5, combustivel: ["Gasolina"], ano: 2020, modelo: "Sei lá", cilindradas: 150, tipoFreio: "Normal", guidao: "Normal", esportiva: false, pedal: "Normal");
        let moto3: Moto = Moto(qtdRodas: 3, cor: "Preta", marca: "Honda", motor: 1.5, combustivel: ["Gasolina"], ano: 2020, modelo: "Sei lá", cilindradas: 150, tipoFreio: "Normal", guidao: "Normal", esportiva: false, pedal: "Normal");
        
        self.motos.append(moto1);
        self.motos.append(moto2);
        self.motos.append(moto3);
        
        moto1.acelerar();
        moto1.freiar();
        print(moto1.qtdRodas);
        print(moto2.qtdRodas);
        print(moto3.qtdRodas);
        
        
        // Do any additional setup after loading the view.
    }
    
    //MARK: Actions
    
    @IBAction func act01(_ sender: UIButton) {
        let carro: Carro = Carro(qtdRodas: 1, cor: "Azul", qtdPortas: 4, marca: "GM", motor: 1.5, combustivel: ["Gasolina"], ano: 1998, modelo: "Monza tubarão", cambioAutomatico: false, cambioManual: true);
        changeLabels(var: carro);
        btn01.isEnabled = false;
        btn02.isEnabled = true;
        btn03.isEnabled = true;
        btn04.isEnabled = true;
    }
    
    @IBAction func act02(_ sender: UIButton) {
        let carro: Carro = Carro(qtdRodas: 2, cor: "Azul", qtdPortas: 4, marca: "GM", motor: 1.5, combustivel: ["Gasolina"], ano: 1998, modelo: "Monza tubarão", cambioAutomatico: false, cambioManual: true);
        changeLabels(var: carro);
        btn01.isEnabled = true;
        btn02.isEnabled = false;
        btn03.isEnabled = true;
        btn04.isEnabled = true;
    }
    
    @IBAction func act03(_ sender: UIButton) {
        let carro: Carro = Carro(qtdRodas: 3, cor: "Azul", qtdPortas: 4, marca: "GM", motor: 1.5, combustivel: ["Gasolina"], ano: 1998, modelo: "Monza tubarão", cambioAutomatico: false, cambioManual: true);
        changeLabels(var: carro);
        btn01.isEnabled = true;
        btn02.isEnabled = true;
        btn03.isEnabled = false;
        btn04.isEnabled = true;
    }
    
    @IBAction func act04(_ sender: UIButton) {
        let carro: Carro = Carro(qtdRodas: 4, cor: "Azul", qtdPortas: 4, marca: "GM", motor: 1.5, combustivel: ["Gasolina", "Alcool"], ano: 1998, modelo: "Monza tubarão", cambioAutomatico: false, cambioManual: true);
        changeLabels(var: carro);
        btn01.isEnabled = true;
        btn02.isEnabled = true;
        btn03.isEnabled = true;
        btn04.isEnabled = false;
    }
    
    @IBAction func sorteandoCarros(_ sender: UIButton) {
        
        btn01.isEnabled = true;
        btn02.isEnabled = true;
        btn03.isEnabled = true;
        btn04.isEnabled = true;
        
        let randomInt = Int.random(in: 0..<2);
        print(randomInt);
        
        if(randomInt==0){
            guard let carroSorteado : Carro = self.carros.randomElement() else {return};
            changeLabels(var: carroSorteado);
        } else {
            guard let motoSorteada : Moto = self.motos.randomElement() else {return};
            changeLabelsMoto(var: motoSorteada);
            print(motoSorteada.qtdRodas);
        }
        
    }
    
    //MARK: Functions
    func changeLabels (var carro: Carro){
        
        guard let _modelo = carro.modelo else {return};
        guard let _motor = carro.motor else {return};
        guard let _cor = carro.cor else {return};
        guard let _qtdPortas = carro.qtdPortas else {return};
        guard let _qtdRodas = carro.qtdRodas else {return};
        guard let _combustivel = carro.combustivel else {return};
        guard let _ano = carro.ano else {return};
        guard let _cambio = carro.cambioManual else {return};
        guard let _marca = carro.marca else {return};
        
        var comb : String = "";
        
        
        self.modeloLabel.text = _modelo;
        self.motorLabel.text = _motor.description;
        self.corLabel.text = _cor;
        self.qtdPortasLabel.text = String(_qtdPortas);
        self.qtdRodasLabel.text = String(_qtdRodas);
        self.anoLabel.text = String(_ano);
        self.marcaLabel.text = _marca;
        if (_cambio) {
            self.cambioLabel.text = "O câmbio do carro é manual";
        } else {
            self.cambioLabel.text = "O câmbio do carro é automático";
        }
        for n in _combustivel {
            comb = comb + n + " - ";
        }
        self.combustivelLabel.text = comb;
    }
    
    func changeLabelsMoto (var moto: Moto){
        
        guard let _qtdRodas = moto.qtdRodas else {return};
        guard let _cor = moto.cor else {return};
        guard let _marca = moto.marca else {return};
        guard let _motor = moto.motor else {return};
        guard let _combustivel = moto.combustivel else {return};
        guard let _ano = moto.ano else {return};
        guard let _modelo = moto.modelo else {return};
        guard let _cilindradas = moto.cilindradas else {return};
        guard let _tipoFreio = moto.tipoFreio else {return};
        guard let _guidao = moto.guidao else {return};
        guard let _esportiva = moto.esportiva else {return};
        
        var comb : String = "";
        
        self.modeloLabel.text = _modelo;
        self.motorLabel.text = _motor.description;
        self.corLabel.text = _cor;
        self.qtdPortasLabel.text = "Nenhuma Porta";
        self.qtdRodasLabel.text = String(_qtdRodas);
        self.anoLabel.text = String(_ano);
        self.marcaLabel.text = _marca;
        self.cambioLabel.text = "Moto não tem câmbio";
        for n in _combustivel {
            comb = comb + n + " - ";
        }
        self.combustivelLabel.text = comb;
        
    }
}

